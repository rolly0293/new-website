<?php
/**
 *
 * Template Name: STATISTIC
 * @package WordPress
 * @subpackage WAH WEBSITE
 * @since WAHSITE 1.0
 */
?>


<?php get_header(); ?>
 <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>

<div class="container">
  <div class="post-feature-img">
  <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
  </div>
    <?php the_post(); ?>
    <div class="entry-content">
      <div class="row dashboard-stat">
        <div class="col-md-4 stat-col ">       
          <div><i class="fa fa-grey fa-hospital-o fa-lg statsheader" aria-hidden="true"></i> Total # of Clinics</div>
          <div class="stat-number counter">153</div>
        </div>
        <div class="col-md-4 stat-col">
          <div><i class="fa fa-grey fa-building-o fa-lg statsheader" aria-hidden="true"></i> Total # of Cities & Municipalities</div>
          <div class="stat-number counter">116</div>
        </div>
        <div class="col-md-4 stat-col">
          <div><i class="fa fa-grey fa-home fa-lg statsheader" aria-hidden="true"></i> Total # of Barangay covered by WAH</div>
          <div class="stat-number counter">2,924</div>
        </div>
         <div class="col-md-4 stat-col">
          <div><i class="fa fa-grey fa-stethoscope fa-lg statsheader" aria-hidden="true"></i> Total # of Patient Consultations</div>
          <div class="stat-number counter">6,561,274</div>
        </div>
         <div class="col-md-4 stat-col">
          <div><i class="fa fa-grey fa-pencil-square-o fa-lg statsheader" aria-hidden="true"></i> Total # of Patients Registered</div>
          <div class="stat-number counter">1,832,073</div>
        </div>
         <div class="col-md-4 stat-col">
          <div><i class="fa fa-grey fa-calendar-check-o fa-lg statsheader" aria-hidden="true"></i> Total # of Patients Served Daily</div>
          <div class="stat-number counter">8,462</div>
        </div>
         <div class="col-md-4 stat-col">
          <div><i class="fa fa-grey fa-mobile fa-lg statsheader" aria-hidden="true"></i> Total # of SMS Sent</div>
          <div class="stat-number counter">22,802</div>
        </div>
         <div class="col-md-4 stat-col">
          <div><i class="fa fa-grey fa-laptop fa-lg statsheader" aria-hidden="true"></i> Total # of Computers Using WAH</div>
          <div class="stat-number counter">899</div>
        </div>
         <div class="col-md-4 stat-col">
          <div><i class="fa fa-grey fa-user-md fa-lg statsheader" aria-hidden="true"></i> Total # of Clinicians Trained</div>
          <div class="stat-number counter">3,066</div>
        </div>
    </div>
    <div class="clearfix"></div>
<!--
    <div class="graph-container">
      <div class="row dashboard-stat">
        <div class="widget-title">WAH Activities - January 2016</div>
        <hr/>
        <div class="col-md-8">
          <img class="img-responsive" src="<?php echo get_bloginfo('template_directory');?>/img/graph-activities.png" /> 
        </div>
        <div class="col-md-4">
          <div class="graph-title">Summary</div>
          <hr/>
          <div class="">
            <div class="stat-item">Patient Registered <span class="total-numbers">5,000</span></div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="70"
                aria-valuemin="0" aria-valuemax="100" style="width:70%">
                  <span class="sr-only">70% Complete</span>
                </div>
              </div>
          </div> 
          <div class="">
            <div class="stat-item">Clinician Trained <span class="total-numbers">10,000</span></div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="70"
                aria-valuemin="0" aria-valuemax="100" style="width:90%">
                  <span class="sr-only">70% Complete</span>
                </div>
              </div>
          </div>
          <div class="">
            <div class="stat-item">SMS Sent <span class="total-numbers">5,000</span></div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="70"
                aria-valuemin="0" aria-valuemax="100" style="width:70%">
                  <span class="sr-only">70% Complete</span>
                </div>
              </div>
          </div> 
          <div class="">
            <div class="stat-item">Unit Installed <span class="total-numbers">5,000</span></div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="70"
                aria-valuemin="0" aria-valuemax="100" style="width:70%">
                  <span class="sr-only">70% Complete</span>
                </div>
              </div>
          </div> 
        </div>
      </div>
      <div class="row dashboard-stat">
        <div class="graph-widget">
          <div class="graph-title">LGU Partners</div>
          <hr/>
          <div id="canvas-doughnat" style="width:100%">
              <canvas id="chart-doughnat"/>
          </div>

        </div>
        <div class="graph-widget">
          <div class="graph-title">Patients Served Daily</div>
          <hr/>
          <img class="img-responsive" src="<?php echo get_bloginfo('template_directory');?>/img/line-chart.png" /> 
        </div>

        <div class="graph-widget">
          <div class="graph-title">Patient Consultations</div>
          <hr/>
          <img class="img-responsive" src="<?php echo get_bloginfo('template_directory');?>/img/bar-chart.png" /> 
        </div>
      </div>
    </div>
  </div>   
   -->       
</div>
<script>
    jQuery(document).ready(function( $ ) {
        $('.counter').counterUp({
            delay: 30,
            time: 3000
        });
    });
</script>


<?php get_footer(); ?>

