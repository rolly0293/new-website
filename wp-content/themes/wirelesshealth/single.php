<?php get_header(); ?>

<div class="container">
    <div class="col-md-12">

     <?php 
                            $feat_image = wp_get_attachment_url(get_post_thumbnail_id());
                          ?>
                         
                    <img src="<?php echo $feat_image; ?>" class="nnn" />
        <?php the_post(); ?>
    
        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
   
            <div class="entry-content">
                <?php the_content(); ?>
                <?php wp_link_pages('before=<div class="page-link">' . __( 'Pages:', 'hbd-theme' ) . '&after=</div>') ?>
            </div><!-- .entry-utility -->
            <div class="nav-below text-right">
                    <?php previous_post_link( '%link', '<span class="meta-nav">&laquo;</span> Prev' ) ?> <span style="color: #fff;">&#8226;</span>  <?php next_post_link( '%link', 'Next <span class="meta-nav ">&raquo;</span>' ) ?>
            </div><!-- #nav-below -->     
            
        </div><!-- #content -->
    </div>
    <div class="col-md-3 sub-header-bg">
   
    </div>
</div><!-- #container -->
<?php get_footer(); ?>
