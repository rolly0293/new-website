<div class="container ">

	<div class="page-title partner">What Our Partners Say</div>
	<div class="col-md-4 col-sm-4 col-xs-10"> </div>
	<div class="sub-content text-center col-md-4 col-sm-4 col-xs-6">Read what our partners are saying. </div>
<div class="col-md-12">

	<?php 
		$args = array('post_type' => 'testimonial', "posts_per_page" => 3);
        $testimonial = new WP_Query($args);
        if ($testimonial->have_posts()):
                while ($testimonial->have_posts()): $testimonial->the_post();
	?>
	<div class="col-md-4">
	<div class="testimonial-item ">
		<div class="testimonial-content"><?php the_content(); ?></div>
		<div class="t-name"><?php the_title(); ?></div>
		<div class="t-position"><?php echo get_post_meta(get_the_ID(), 'wpcf-position', true); ?></div>
		<div class="t-location partnerpad"><?php echo get_post_meta(get_the_ID(), 'wpcf-location', true); ?></div>
	</div>
</div>
<?php 
  endwhile;
  endif;
 ?>

</div>
</div>