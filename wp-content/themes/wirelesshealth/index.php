<?php get_header(); ?>


<section>
	<!-- CAROUSEL -->
	<?php get_template_part( 'content', 'carousel' ); ?>
</section>
<section class="section">
	<!-- HOME WIDGET -->
	<?php get_template_part( 'content', 'homewidget' ); ?>
</section>
 <section class="section grey-page" data-type="background" data-speed="10">
 	<?php get_template_part( 'content', 'about' ); ?>
</section>

 <section class="section">
	<?php get_template_part( 'content', 'post' ); ?>
</section>
 <section class="section grey-page" data-type="background" data-speed="10">
 	<?php get_template_part( 'content', 'top-performer' ); ?>
</section>
 <section class="section">
 	<?php get_template_part( 'content', 'partners' ); ?>
 	</section>
 	
<?php get_footer(); ?>
