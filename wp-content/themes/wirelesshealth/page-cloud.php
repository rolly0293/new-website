<?php
/**
*
* Template Name: WAH Milestones
* @package WordPress
* @subpackage WAH WEBSITE
* @since WAHSITE 1.0
*/
?>


<?php get_header(); ?>


<div class="container">
<div class="sub-header-bg post-feature-img">
<?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
</div>
    <?php the_post(); ?>
    <div class="entry-content">
        <p><?php echo the_content();?></p>
        <div class="clearfix"></div>
		<div class="container">
		    <div class="row">
		        <div class="level ">
		        	<?php 
					$args = array('post_type' => 'wah-milestone', "posts_per_page" => -1, 'order' => 'ASC');
			        $milestone = new WP_Query($args);
			        $profile = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID(), 'full'));
			        ?>
		            <div class="board-inner ">
		            <ul class="nav nav-tabs milestone-bg-tabs " id="milestone">
		            <div class="liner"></div>
			        <?php
			        $i = 0;
			        if ($milestone->have_posts()):
			                while ($milestone->have_posts()): $milestone->the_post();
			            
					?>
		            <li class="milestone-tab" <?php if($i == 0){ echo 'class="active"';}?>>
		             <a href="#<?php echo $post->post_name;?>" data-toggle="tab" >
		              <div class="round-tabs hvr-pulse">
		                    <i class=" fa-services fa  <?php echo get_post_meta(get_the_ID(), 'wpcf-milestone-icon', true); ?>"></i>
		        
		              </div> 
		          	</a>
		          	<div class="service-item-title"><?php the_title();?></div>
		      		</li>
		      		<?php $i++; ?>
		            <?php 
					  endwhile;
					  endif;
					 ?>
		             </ul>
		         	</div>

		            <div class="tab-content">
		            <?php
		            $i = 0;
			        if ($milestone->have_posts()):
			                while ($milestone->have_posts()): $milestone->the_post();
					?>
		              <div class="tab-pane fade <?php if($i == 0){ echo 'in active'; }?>" id="<?php echo $post->post_name;?>">
		              		<div class="service-title"><?php echo get_post_meta(get_the_ID(), 'wpcf-milestone-title', true); ?></div>
		              		
							<div class="col-md-5 col-sm-10 col-xs-12 services-img">
		              			<?php the_post_thumbnail('full', array('class' => 'img-responsive service-img')); ?>
		              		</div>
		              		<div class="col-md-7 col-sm-7 col-xs-12 services-field-content">
		              			<div class="service-content"><?php the_content(); ?></div>
		              			<table class="table service-table">
					              <tbody>
					                <tr>
					                  <td>Partnerships Forged:</td>
					                  <td>
					                  	<div><?php echo get_post_meta(get_the_ID(), 'wpcf-partners-forged', true); ?></div>
					                  </td>
					                </tr>
					                <tr>
					                  <td>Number of New Sites:</td>
					                  <td>
					                  	<div><?php echo get_post_meta(get_the_ID(), 'wpcf-number-of-new-sites', true); ?></div>
					                  </td>
					                </tr>
					            	
					                <tr>
					                  <td>Technology:</td>
					                  <td>
					                    <div><?php echo get_post_meta(get_the_ID(), 'wpcf-technology', true); ?></div>
					                  </td>
					                </tr>
					               
					              </tbody>
					            </table>

		              		</div>

		              </div>
		              <?php $i++; ?>
		            <?php 
					  endwhile;
					  endif;
					?>
					<div class="clearfix"></div>
					</div>

				</div>
			</div>
		</div>
    </div> 



   


</div>




								<?php get_footer(); ?>
