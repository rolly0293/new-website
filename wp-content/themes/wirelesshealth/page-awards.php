<?php
/**
 *
 * Template Name: Awards
 * @package WordPress
 * @subpackage WAH WEBSITE
 * @since WAHSITE 1.0
 */
?>



<?php get_header(); ?>
<div class="container">

  <?php the_post(); ?>
<div class="container">
  <?php
    $args = array(
      'post_type' => 'award',
      'order' => 'DESC'
      );

    $widgets = new WP_Query($args);
    if ($widgets->have_posts()):
      while ($widgets->have_posts()): $widgets->the_post();
    {
      ?>
<div class="col-md-3">
  
       
              <?php the_post_thumbnail('medium', array('class' => 'img-responsive about-widget-img')); ?>
           
            
        </div>
          
      

      <?php
    }
    endwhile;
    endif;
    ?>

<div class="clearfix"></div>

<hr>
<div class="container">
  <?php
    $args = array(
      'post_type' => 'conference',
      );

    $widgets = new WP_Query($args);
    if ($widgets->have_posts()):
      while ($widgets->have_posts()): $widgets->the_post();
    {
      ?>
<div class="col-md-4">
  
       
              <?php the_post_thumbnail('medium', array('class' => 'img-responsive about-widget-img')); ?>
  
            
        </div>
          
      

      <?php
    }
    endwhile;
    endif;
    ?>
</div>
</div>


<?php get_footer(); ?> 