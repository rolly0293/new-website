<div class="container">
	<div class="col-md-12">
	<div class="page-title">WAH UPDATES</div>
	<div class="sub-content text-center">Keep abreast of what is happening with your #wahteam and your #wahcommunity. </div>
	<div class="title-space">

		<?php 
		$the_query = new WP_Query( 'posts_per_page=4' ); 
		$count=1;
		while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
			
			<div class="post-inner-content post-<?php echo $count++; ?>">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="hvr-hang post-img"><?php echo get_the_post_thumbnail( get_the_ID(), 'thumbnail' ); ?></div>
					<div class="post-excerpt wow fadeInUp" data-wow-duration="2s">
						<div class="entry-title spaced"><?php the_title(); ?></div>
						<div class="entry-date"><?php the_date(); ?></div>
						<div class="entry-excerpt">
							<?php the_excerpt(__('(more…)')); ?>
							<div class="readmore-color"> <div class="fa fa-eye iconcolor"></div><a href="<?php the_permalink(); ?>">  Read more</a>
						</div>
					</div>
						
				</article>
			</div>

		<?php endwhile; ?>

		</div>
	
	</div>
</div>
