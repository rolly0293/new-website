<?php
/**
 *
 * Template Name: CONTACT US
 * @package WordPress
 * @subpackage WAH WEBSITE
 * @since WAHSITE 1.0
 */
?>



<?php get_header(); ?>
 

<div class="container">
  <div class="post-feature-img">
    <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
  </div>
  <?php the_post(); ?>
  <div class="entry-content">
    <div class="row dashboard-stat">
      <div class="col-md-12 contactusadd">       
        <div><i class="fa fa-grey fa-map-marker fa-2x fa-align-center statsheader" aria-hidden="true"></i>  Rm. 201 TPH Dormitory, Hospital Drive, San Vicente, Tarlac City 2300, Philippines</div>
         <div class="col-md-6 infocontactus">
        <div><i class="statsheader" aria-hidden="true"></i> <b>General Inquiries </b></div>

      </div>
      <div class="col-md-6 infocontactus1">
        <div><i class="fa fa-grey fa-envelope-o fa-lg statsheader" aria-hidden="true"></i>  Email: <u class="mailcolor">info@wah.ph</u></div>
        <div><i class="fa fa-grey fa-phone fa-lg statsheader" aria-hidden="true"></i>  Phone: <u class="mailcolor">(045) 982-1246</u></div>
      </div>
       <div class="col-md-6 infocontactus">
        <div><i class="statsheader" aria-hidden="true"></i> <b>Billing and Contracts Inquiries</b></div>

      </div>
      <div class="col-md-6 infocontactus1">
        <div><i class="fa fa-grey fa-envelope-o fa-lg statsheader" aria-hidden="true"></i>  Rose Ann Biag  | <u class="mailcolor">roseann.biag@wah.ph</u></div>
        <div><i class="fa fa-grey fa-envelope-o fa-lg statsheader" aria-hidden="true"></i> Jhuvy Dizon  |<u class="mailcolor"> jhuvydizon@wah.ph </u></div>
      </div>

      <div class="col-md-6 infocontactus4">	
	 <div><i class="statsheader" aria-hidden="true"></i> <b>Training Inquiries </b></div>
	  </div>
       <div class="col-md-6 infocontactus3">
       
  	<div><i class="statsheader" aria-hidden="true"></i> <b>Regions 1 & 2 </b> <div><i class="fa fa-grey fa-envelope-o fa-lg statsheader" aria-hidden="true"></i> Jayson Cabanero and Myra Pangilinan  |<u class="mailcolor">jaycabanero2@gmail.com | myrajoydpangilinan@wah.ph</u></div></div>

	<div><i class="statsheader" aria-hidden="true"></i> <b>Regions 3 & 4a </b><div><i class="fa fa-grey fa-envelope-o fa-lg statsheader" aria-hidden="true"></i> Izel Mar Sese & Kristian John Cabansag  |<u class="mailcolor">izelmar.sese@gmail.com | kjscab@gmail.com</u></div></div>

	<div><i class="statsheader" aria-hidden="true"></i> <b>Region 4b & 5 </b><div><i class="fa fa-grey fa-envelope-o fa-lg statsheader" aria-hidden="true"></i> Anna Katrina Yturralde  |<u class="mailcolor">annakat.yturralde@wah.ph</u></div></div>

	<div><i class="statsheader" aria-hidden="true"></i> <b>Visayas </b><div><i class="fa fa-grey fa-envelope-o fa-lg statsheader" aria-hidden="true"></i> Kim Chino David  |<u class="mailcolor">kimchinodavid@wah.ph</u></div></div>

	<div><i class="statsheader" aria-hidden="true"></i> <b>Mindanao </b> <div><i class="fa fa-grey fa-envelope-o fa-lg statsheader" aria-hidden="true"></i> Francis Joseph Gamboa |<u class="mailcolor">francis.gamboa@wah.ph</u></div></div>
	</div>
<style>.infocontactus3{
     margin-top: 30px;
    margin-bottom: 20px;
    text-align: left;
    font-size: 18px;
    padding-top: 25px;
     padding-bottom: 40px;
	border-left: 2px solid #8c8c8c;
}
  .infocontactus4{
    margin-top: 100px;
    margin-bottom: 20px;
    text-align: right;
    font-size: 20px;
    padding-top: 40px;
     padding-bottom: 40px;

}
</style>
       <div class="col-md-6 infocontactus">
        <div><i class="statsheader" aria-hidden="true"></i> <b>Technical Inquiries </b></div>

      </div>
      <div class="col-md-6 infocontactus1">
        <div><i class="fa fa-grey fa-envelope-o fa-lg statsheader" aria-hidden="true"></i> Macquel Serrano  | <u class="mailcolor">macquelmserrano@wah.ph</u></div>
  <div><i class="fa fa-grey fa-envelope-o fa-lg statsheader" aria-hidden="true"></i> Kevin Greg Alvarado  | <u class="mailcolor">kgalvarado83187@gmail.com</u></div>
      </div>
      </div>
       <div class="col-md-6 infocontactus">
        <div><i class="statsheader" aria-hidden="true"></i> <b>Media Inquiries </b></div>

      </div>
      <div class="col-md-6 infocontactus1">
        <div><i class="fa fa-grey fa-envelope-o fa-lg statsheader" aria-hidden="true"></i> Precious Joan Narciso  | <u class="mailcolor">joannarciso@wah.ph</u></div>
        <div><i class="fa fa-grey fa-envelope-o fa-lg statsheader" aria-hidden="true"></i> Maria Cristiana Santos  |<u class="mailcolor">mariacristiana@gmail.com</u></div>
      </div>
       

     </div>   
  </div>      
</div>
<div class="clearfix"></div>
<?php get_footer(); ?>

