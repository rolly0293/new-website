<!DOCTYPE html>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?php wp_title(); ?>
    </title>

    <?php wp_head(); ?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.min.js"></script>

</head>

<body>
    <div class="main-content"> 
        <div class="top-header">
            <div class="container mobile">
                <div>Contact Us: (045) 982-1246 / <a class="mail" href="https://www.wah.pilipinas@gmail.com"> info@wah.ph </a></div>
                <div><ul class="top-social-icons">
                    <li><a target="_blank"  href="https://www.facebook.com/wah.ph/"><i class="fa fa-facebook fa-top-header" aria-hidden="true"></i></a></li>
                    <li><a target="_blank"  href="https://twitter.com/wah_team/"><i class="fa fa-twitter fa-top-header" aria-hidden="true"></i></a></li>
                    <li><a target="_blank" href="http://www.wah.pilipinas@gmail.com">
<i class="fa fa-envelope fa-top-header" aria-hidden="true"></i></a></li>
                    <li><a target="_blank"  href="https://www.linkedin.com/company/6594007?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A6594007%2Cidx%3A3-1-6%2CtarId%3A1473903817787%2Ctas%3AWireless%20Access%20"><i class="fa fa-linkedin fa-top-header" aria-hidden="true"></i></a></li>
                  
                    <li class="hide-dash-logout"><span>| </span><?php if ( !is_user_logged_in() ) { ?> <i class="fa fa-lock" aria-hidden="true"></i> <?php } ?>
                        <a <?php if ( !is_user_logged_in() ) { ?> data-toggle="modal" data-target="#login" <?php }else{ echo "href='http://dashboard.wah.ph/'"; } ?> class="dashboard-link mail"> Dashboard</a> <?php if ( is_user_logged_in() ) { ?> | <a class="mail" href="<?php echo wp_logout_url(get_permalink()); ?>">Logout</a><?php } ?></li>



                    
                    </ul>
                </div>
            </div>
        </div>
        <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="loginLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content  modalsize">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="loginLabel"> Please Visit WAH Dashboard for Statistics </h4>
              </div>
              <div class="modal-body">
              
                    <p>or please click the link below</p>
              <a target="_blank" href="http://dashboard.wah.ph/"><i class="fa fa-bar-chart">
               dashboard.wah.ph</i></a>
              </div>
            </div>
          </div>
        </div>


        <header class="container  site-header ">
            <a href="<?php echo site_url(); ?>">
                <img src="<?php echo get_bloginfo('template_directory');?>/img/wah.png" class="wah-logo hvr-pulse-grow" />
            </a>
                                <nav id="site-navigation" class="main-navigation" role="navigation">
                                    <a class="menu-toggle nav navbar">
                                        <?php _e('Menu', 'accesspress-store'); ?>
                                    </a>
                                    <?php
                                        wp_nav_menu(array(
                                            'theme_location' => 'primary',
                                            'container_class' => 'main-menu',
                                            'fallback_cb' => 'accesspress_store_fallback_menu',

                                        ));
                                    ?>
                                </nav><!-- #site-navigation -->
           

        </header>
        <div id="main-content">
            <?php if( ! is_front_page() || ! is_home() ) : ?>
            <hr/>
            <div class="container sub-header-bg">
            <div class="page-title"><?php                           
            if ( is_category() ) :                            
                single_cat_title();
            elseif ( is_tag() ) :
                single_tag_title();
            else:
                the_title();
            endif;
             ?>
         </div>
         <?php if(is_page()){ ?>
         <div class="container intro-description"><?php echo get_post_meta(get_the_ID(), 'wpcf-subtitle', true) ?></div>
         <?php }?>
         
           </div> 
            <?php endif; ?>

 