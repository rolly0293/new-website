

<div class="container test">
	<div class="page-title">Our Partners</div>
	
	<ul class="bxlsider">
	<div class="title-space partners">
		<?php
		    $args = array(
		        'post_type' => 'partner',
		        'order' => 'ASC'
		    );
		    $widgets = new WP_Query($args);
		    if ($widgets->have_posts()):
		    while ($widgets->have_posts()): $widgets->the_post();
		        $feat_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID(), 'full'));
		        ?>
		        <div class="">                   
		            <img class="img-responsive hvr-pulse-shrink" src="<?php echo $feat_image[0]; ?>"/>  

		        </div>
		        <?php
		    endwhile;
			endif;
		?>
	</div>
	</div>

	<body style="padding:0px; margin:0px; background-color:#fff;font-family:helvetica, arial, verdana, sans-serif">

    <!-- #region Jssor Slider Begin -->
    <!-- Generator: Jssor Slider Maker -->
    <!-- Source: http://www.jssor.com -->
    <!-- This code works with jquery library. -->
   
    <script type="text/javascript">
        jQuery(document).ready(function ($) {

            var jssor_1_options = {
              $AutoPlay: true,
              $Idle: 0,
              $AutoPlaySteps: 4,
              $SlideDuration: 2500,
              $SlideEasing: $Jease$.$Linear,
              $PauseOnHover: 4,
              $SlideWidth: 140,
              $Cols: 7
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*responsive code begin*/
            /*you can remove responsive code if you don't want the slider scales while window resizing*/
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1000);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            /*responsive code end*/
        });
    </script>
    <style></style>
    <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1000px; height: 100px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px; width:100px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
           

        </div>
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 980px; height: 100px; overflow: hidden;">


            <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/alilem.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/anao.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/bacolod.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/baguiocity.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/bamban.png" width="80" height="80" />
            </div>
            <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/bani.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/bantay.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/basista.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/bayambang.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/bayog.png" width="80" height="80" />
            </div>
            <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/binmaley.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/brookespoint.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/burgos.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/cagwait.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/cajidiocan.png" width="80" height="80" />
            </div>
            <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/calasiao.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/camiling.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/candon.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/caoayan.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/capas.png" width="80" height="80" />
            </div>
            <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/cataingan.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/cervantes.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/concepcion.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/cortes.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/dao.png" width="80" height="80" />
            </div>
            <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/daram.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/datupaglas.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/delcarmen.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/dsb.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/dumalneg.png" width="80" height="80" />
            </div>
            <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/gamay.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/gandara.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/gerona.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/gregorio.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/guipos.png" width="80" height="80" />
            </div>
            <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/gutalac.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/hinatuan.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/imelda.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/ipil.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/kapatagan.png" width="80" height="80" />
            </div>
            <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/laoac.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/lapaz.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/lapuyan.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/lavezares.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/leon.png" width="80" height="80" />
            </div>
            <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/libon.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/lidlidda.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/liloy.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/mabitac.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/magdiwang.png" width="80" height="80" />
            </div>
            <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/malasiqui.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/mandaon.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/mangaldan.png" width="80" height="80" />
            </div>
           
            
            <a data-u="any" href="http://www.jssor.com" style="display:none">Scrolling Logo Thumbnail Slider</a>
               <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/manukan.png" width="80" height="80" />
            </div>
              <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/margosatubig.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/matuguinao.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/mayantoc.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/milagros.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/moncada.png" width="80" height="80" />
            </div>
            <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/motiong.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/naga.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/narra.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/upi.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/pagsanghan.png" width="80" height="80" />
            </div>
            <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/panglimasugala.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/paniqui.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/pitogo.png" width="80" height="80" />
            </div>
              <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/pilar.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/puertoprinsesa.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/pura.png" width="80" height="80" />
            </div>
            <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/quirino.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/ramos.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/roxas.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/rtlim.png" width="80" height="80" />
            </div>
              <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/sanantonio.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/sanclemente.png" width="80" height="80" />
            </div>
            <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/sanfernandocamsur.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/sanfernandoromblon.png" width="80" height="80" />
            </div>
              <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/sanildefonso.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/sanjose.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/sanmanuelpang.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/sanmigueltarlac.png" width="80" height="80" />
            </div>
            <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/sanmiguelzds.png" width="80" height="80" />
            </div>
              <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/santacruz.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/sanpablo.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/sansebastian.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/santa.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/santol.png" width="80" height="80" />
            </div>
            <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/sergio.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/sigay.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/sindangan.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/siniloan.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/sjdb.png" width="80" height="80" />
            </div>
            <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/staignacia.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/stodomingo.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/sual.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/sugpon.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/tagapulan.png" width="80" height="80" />
            </div>
            <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/tambulig.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/tandag.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/tinambac.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/titay.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/tungawan.png" width="80" height="80" />
            </div>
            <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/upi.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/urdaneta.png" width="80" height="80" />
            </div>

             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/victoria.png" width="80" height="80" />
            </div>
            <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/vigan.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/villareal.png" width="80" height="80" />
            </div>
             <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/villasis.png" width="80" height="80" />
            </div>
            <div>
                <img data-u="" src="<?php echo get_bloginfo('template_directory');?>/logo/vsagun.png" width="80" height="80" />
            </div>

           
        </div>
    </div>
    <!-- #endregion Jssor S

</div>