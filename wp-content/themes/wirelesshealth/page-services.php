<?php
/**
 *
 * Template Name: services
 * @package WordPress
 * @subpackage WAH WEBSITE
 * @since WAHSITE 1.0
 */
?>


<?php get_header(); ?>
 


<div class="container-fluid no-padding">

		<ul class="bxslider">
			<?php 
				$args = array('post_type' => 'service', 'orderby' => 'date','order' => 'DESC');
		        $slides = new WP_Query($args);
		        if ($slides->have_posts()):
		                while ($slides->have_posts()): $slides->the_post();
			?>
			  <li>
			  	<?php 
					$feat_image = wp_get_attachment_url(get_post_thumbnail_id());
				?>
			
				<div class="header-services">
				        <div class="text-center"><?php the_title(); ?></div>
				        </div>
				<div class="">
			  	<img src="<?php echo $feat_image; ?>" class="nnn"/></div>
			  	  
			  </li>
		  <?php 
		  endwhile;
		  endif;
		  ?>
		</ul>
</div>

			<div class="clearfix"> </div>


 
<?php get_footer(); ?>