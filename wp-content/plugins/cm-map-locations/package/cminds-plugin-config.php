<?php

use com\cminds\maplocations\App;

$cminds_plugin_config = array(
	'plugin-is-pro'				 => App::isPro(),
	'plugin-has-addons'      => TRUE,
	'plugin-addons'        => array(
		array(
			'title' => 'CM Map Routes Manager',
			'description' => 'Draw map routes and generate a catalog of routes and trails with points of interest using Google maps. ',
			'link' => 'https://www.cminds.com/store/maps-routes-manager-plugin-for-wordpress-by-creativeminds/',
			'link_buy' => 'https://www.cminds.com/checkout/?edd_action=add_to_cart&download_id=54033&wp_referrer=https://www.cminds.com/checkout/&edd_options[price_id]=1'
		),
		array(
			'title' => 'CM Business Directory',
			'description' => 'Build online business directory. Let WordPress users post and manage listings. Includes payment support.',
			'link' => 'https://www.cminds.com/store/purchase-cm-business-directory-plugin-for-wordpress/',
			'link_buy' => 'https://www.cminds.com/checkout/?edd_action=add_to_cart&download_id=33301&wp_referrer=https://www.cminds.com/checkout/&edd_options[price_id]=1'
		),
	),
	'plugin-version'			 => App::VERSION,
	'plugin-abbrev'				 => App::PREFIX,
	'plugin-parent-abbrev'		 => '',
	'plugin-affiliate'               => '',
	'plugin-redirect-after-install'  => admin_url( 'admin.php?page=cmloc-settings' ),
	'plugin-show-guide'                 => TRUE,
	'plugin-guide-text'                 => '    <div style="display:block">
	<ol>
	<li>Go to the plugin <strong>"Setting"</strong></li>
	<li>Get a  <strong>Google Maps Server APP Key</strong> and add it to the plugin settings. </li>
	<li>From the plugin settings click on the <strong>user dashboard</strong> link</li>
	<li>Add your first location. You need to pin the location on the Google map and you can also add a description and images.</li>
	<li><strong>View</strong> the location</li>
	<li>In the <strong>Plugin Settings</strong> you can click on the link of all locations to view them all on one map.</li>
	<li><strong>Troubleshooting:</strong> Make sure that you are using Post name permalink structure in the WP Admin Settings -> Permalinks.</li>
	<li><strong>Troubleshooting:</strong> If post type archive does not show up or displays 404 then install Rewrite Rules Inspector plugin and use the Flush rules button.</li>
	<li><strong>Troubleshooting:</strong> Get the Google Maps Server APP Key. Plugin will not work without it.</li>
	</ol>
	</div>',
	'plugin-guide-video-height'         => 240,
	'plugin-guide-videos'               => array(
		array( 'title' => 'Installation tutorial', 'video_id' => '160220318' ),
	),
	'plugin-show-shortcodes'	 => TRUE,
	'plugin-shortcodes'			 => '<p>You can use the following available shortcodes.</p>',
	'plugin-shortcodes-action'	 => 'cmloc_display_supported_shortcodes',
	'plugin-settings-url' 		 => admin_url( 'admin.php?page=cmloc-settings' ),
	'plugin-file'				 => App::getPluginFile(),
	'plugin-dir-path'			 => plugin_dir_path( App::getPluginFile() ),
	'plugin-dir-url'			 => plugin_dir_url( App::getPluginFile() ),
	'plugin-basename'			 => plugin_basename( App::getPluginFile() ),
	'plugin-icon'				 => '',
	'plugin-name'				 => App::PLUGIN_NAME,
	'plugin-license-name'		 => App::PLUGIN_NAME,
	'plugin-slug'				 => App::PREFIX,
	'plugin-short-slug'			 => App::PREFIX,
	'plugin-parent-short-slug'	 => '',
	'plugin-menu-item'			 => App::PREFIX,
	'plugin-textdomain'			 => '',
	'plugin-userguide-key'		 => '568-cm-map-locations-cmml',
	'plugin-store-url'			 => 'https://www.cminds.com/store/map-locations-plugin-for-wordpress-by-creativeminds/',
	'plugin-support-url'		 => 'https://wordpress.org/support/plugin/cm-map-locations',
	'plugin-review-url'			 => 'http://wordpress.org/support/view/plugin-reviews/cm-map-locations',
	'plugin-changelog-url'		 => 'https://www.cminds.com/store/map-locations-plugin-for-wordpress-by-creativeminds/#changelog',
	'plugin-licensing-aliases'	 => App::getLicenseAdditionalNames(),
	'plugin-compare-table'	 => '<div class="pricing-table" id="pricing-table">
	<ul>
	<li class="heading">Current Edition</li>
	<li class="price">$0.00</li>
	<li class="noaction"><span>Free Download</span></li>
	<li>Place locations on Google map</li>
	<li>Add description and images to each location</li>
	<li>Show all locations on a map</li>
	<li>X</li>
	<li>X</li>
	<li>X</li>
	<li>X</li>
	<li>X</li>
	<li>X</li>
	<li>X</li>
	<li>X</li>
	<li>X</li>
	<li>X</li>
	<li>X</li>
	<li>X</li>
	<li>X</li>
	<li>X</li>
	<li class="price">$0.00</li>
	<li class="noaction"><span>Free Download</span></li>
	</ul>

	<ul>
	<li class="heading">Pro</li>
	<li class="price">$29.00</li>
	<li class="action"><a href="https://www.cminds.com/store/map-locations-plugin-for-wordpress-by-creativeminds/" style="background-color:darkblue;" target="_blank">More Info</a> &nbsp;&nbsp;<a href="https://www.cminds.com/?edd_action=add_to_cart&download_id=65188&wp_referrer=https://www.cminds.com/checkout/&edd_options[price_id]=1" target="_blank">Buy Now</a></li>
	<li>Place locations on Google map</li>
	<li>Add description and images to each location</li>
	<li>Show all locations on a map</li>
	<li>Locations index view templates</li>
	<li>Categories Support</li>
	<li>Tags Support</li>
	<li>Search location description</li>
	<li>Search by zip or city or state</li>
	<li>Shortcodes support</li>
	<li>Weather information</li>
	<li>Customize Labels</li>
	<li>Import / Export</li>
	<li>Access Control</li>
	<li>Use custom icons per each location</li>
	<li>Upload your own icons</li>
	<li>Integrates with Routes Manager</li>
	<li>Integrates with Business Directory</li>
	<li class="price">$29.00</li>
	<li class="action"><a href="https://www.cminds.com/store/map-locations-plugin-for-wordpress-by-creativeminds/" style="background-color:darkblue;" target="_blank">More Info</a> &nbsp;&nbsp;<a href="https://www.cminds.com/?edd_action=add_to_cart&download_id=65188&wp_referrer=https://www.cminds.com/checkout/&edd_options[price_id]=1" target="_blank">Buy Now</a></li>
	</ul>

	</div>',
);

