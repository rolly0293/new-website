<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wahsite');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'wahdev');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WP_HOME','http://wah.ph');
define('WP_SITEURL','http://wah.ph');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '>ZK-}5R.83@&V+~[e?gEZzw[E}w1.y9||;b2Z;HOgI9|7-d{R}_Ie+|{_Z:Kl)B@');
define('SECURE_AUTH_KEY',  'Ea-J<zZ;52tl%s_>Oz( 3V5+2?pf_qshXC6n&Os1/^R?[7l]&FYlL6Iie1O7`$%p');
define('LOGGED_IN_KEY',    ')%WN%6v4dk=_~ece]mF$< 4wBC0 {k;!NsaX{]TD8</Xj-0(|q?{^PGDj-:g:Tdt');
define('NONCE_KEY',        'v[2a8DSxi9[tLsl~A;UoVi?@tHwp/~P54pN6.l@;WPat8odi61|LxaZ;}7mvBxv3');
define('AUTH_SALT',        'T:>`NzaQ%qqV=Rkg$Ee15;0kd.uw@Y{]It*2Rak^![gR@20&VyI-VMy6l$--:{Cr');
define('SECURE_AUTH_SALT', 'IIv_;9CGtb:K6KErcdL71K`G 6A{xAw%$0gqBvorodVyU~-eVYF,&+wf@jb+ZD%=');
define('LOGGED_IN_SALT',   '|7|$xGnteA:)@KN?_5mDpC(fnS43i-,i$v%k|#)G-f X200lW!d9n4~1F{YsY&K^');
define('NONCE_SALT',       'T@8AUyRuRM0xygp5&UD$;*uc7ACef`zUw?0^{%Yk,NO1cB?FW^u?SLjj.-1P[@{C');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */


/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
